import numpy as np
import time
import dlib
import cv2
from scipy.spatial import distance
from imutils import face_utils

EYE_ASPECT_RATIO_THRESHOLD = 0.3     #setting the threshold for the eye detection for representing drowsiness
EYE_ASPECT_RATIO_CONSEC_FRAMES = 50    #seeting the consecutive frames 

COUNTER = 0

face_cascade = cv2.CascadeClassifier("haarcascade_frontalface_default.xml")    #haarcascade xml file for detecting face

def eye_aspect_ratio(eye):
    A = distance.euclidean(eye[1],eye[5])
    B = distance.euclidean(eye[2],eye[4])
    C = distance.euclidean(eye[0],eye[3])

    ear = (A+B)/(2*C)
    return ear


#LOAD FACE DETECTOR AND PREDICTOr USING dlib SHAPE PREDICTOR FILE
detector = dlib.get_frontal_face_detector()
predictor = dlib.shape_predictor('shape_predictor_68_face_landmarks.dat')    #shape predictor for landmarks detection

(lStart,lEnd) = face_utils.FACIAL_LANDMARKS_IDXS['left_eye']
(rStart,rEnd) = face_utils.FACIAL_LANDMARKS_IDXS['right_eye']

#Start webcam video capture
video_capture = cv2.VideoCapture(0)

time.sleep(2)

while(True):
    ret,frame = video_capture.read()
    frame = cv2.flip(frame,1)
    gray = cv2.cvtColor(frame,cv2.COLOR_BGR2GRAY)

    #Detect facial points through detector function
    faces = detector(gray,0)



#creating bounding box across faces
    face_rectangle = face_cascade.detectMultiScale(gray,1.3,5)

    for (x,y,w,h) in face_rectangle:
        cv2.rectangle(frame,(x,y),(x+w,y+h),(255,0,0),2)


#detecting the eye inside the face
    for face in faces:
        shape = predictor(gray,face)
        shape = face_utils.shape_to_np(shape)

        leftEye = shape[lStart:lEnd]
        rightEye = shape[rStart:rEnd]


        leftEyeAspectRatio = eye_aspect_ratio(leftEye)
        rightEyeAspectRatio = eye_aspect_ratio(rightEye)

        eyeAspectRatio = (leftEyeAspectRatio + rightEyeAspectRatio)/2

        leftEyeHull = cv2.convexHull(leftEye)
        rightEyeHull = cv2.convexHull(rightEye)

        cv2.drawContours(frame,[leftEyeHull],-1,(0,255,0),1)
        cv2.drawContours(frame,[rightEyeHull],-1,(0,255,0),1)

        if eyeAspectRatio<EYE_ASPECT_RATIO_THRESHOLD:
            COUNTER+=1
            if COUNTER>=EYE_ASPECT_RATIO_CONSEC_FRAMES:
                cv2.putText(frame,"You are drowsy",(150,200),cv2.FONT_HERSHEY_SIMPLEX,1.5,(0,0,255),2)
        else:
            COUNTER = 0

    cv2.imshow('Video',frame)
    if(cv2.waitKey(1) & 0xFF == ord('q')):
        break

video_capture.release()
cv2.destroyAllWindows()

        